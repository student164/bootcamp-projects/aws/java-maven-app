
## ON BRANCH JENKINS-JOBS


### Complete the CI/CD Pipeline (Docker-Compose, Dynamic-versioning)

Goals
---

Complete the CI/CD Pipeline (Docker-Compose, Dynamic-versioning)


    * CI step: Increment version 
    * CI step: Build artifact for Java Maven application
    * CI step: Build and push Docker image to Docker Hub
    * CD step: Deploy new application version with Docker Compose
    * CD step: Commit the version update

1. Step: 
Update jenkinsfile with steps for 'version increment' & 'commit to remote repo'
    * `Incremented app version is assigned from pom.xml build file to IMAGE_NAME in increment version stage.`
    * `Pipeline pushes incremented version pom.xml file back to remote repo in commit stage`



Review other branches for other use cases 
---